package com.zinovand.githubclient.app.presentation.screen.organization

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.zinovand.githubclient.R
import com.zinovand.githubclient.app.presentation.MainActivity
import com.zinovand.githubclient.app.presentation.screen.user.UserDetailsAdapter
import com.zinovand.githubclient.app.presentation.viewmodel.organization.OrganizationViewModelImpl
import com.zinovand.githubclient.app.presentation.viewmodel.user.UserViewModelImpl

/**
 * @author Zinovyev Andrey
 */
class OrganizationFragment : Fragment() {

    lateinit var recyclerView: RecyclerView
    lateinit var listAdapter: OrganizationListAdapter
    lateinit var swiper: SwipeRefreshLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_organization, container, false)
        val viewModel = ViewModelProvider(this).get(OrganizationViewModelImpl::class.java)
        recyclerView = view.findViewById(R.id.rv_organization)
        listAdapter = OrganizationListAdapter()
        recyclerView.adapter = listAdapter
        (activity as MainActivity).actionBarBackEnabled(false)
        (activity as MainActivity).setActionBarTitle("Organizations")

        viewModel.getOrganizations()

        swiper = view.findViewById(R.id.swipe_organization)
        swiper.setOnRefreshListener {
            viewModel.getOrganizations()
            swiper.isRefreshing = false
        }


        viewModel.organizationsList.observe(viewLifecycleOwner, {
            listAdapter.update(it)
        })

        return view
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).setActionBarTitle("Organizations")
    }
}