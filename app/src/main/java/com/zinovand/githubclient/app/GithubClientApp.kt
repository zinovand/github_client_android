//package com.zinovand.githubclient.app
//
//import android.app.Application
//
///**
// * App entry point
// * This class must be specified in android manifest
// *
// * @author Zinovyev Andrey
// */
//class GithubClientApp : Application() {

//    val GITHUB_URL = "https://api.github.com"

    //Not need if use DI
//    lateinit var remoteRepository: RemoteRepository

//    override fun onCreate() {
//        super.onCreate()
//
//       // configureRemoteRepository()
//    }


    //Not need if use DI
//    private fun configureRemoteRepository(): RemoteRepository{
//        val httpLoggingInterceptor = HttpLoggingInterceptor()
//        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
//
//        val okHttpClient = OkHttpClient.Builder()
//            .addInterceptor(httpLoggingInterceptor)
//            .build()
//
//        val retrofit = Retrofit.Builder()
//            .baseUrl(GITHUB_URL)
//            .client(okHttpClient)
//            .addConverterFactory(GsonConverterFactory.create())
//            .build()
//
//        return retrofit.create(RemoteRepository::class.java)
//    }
//}