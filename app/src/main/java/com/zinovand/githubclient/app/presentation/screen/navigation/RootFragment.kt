package com.zinovand.githubclient.app.presentation.screen.navigation

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.zinovand.githubclient.R

/**
 * @author Zinovyev Andrey
 */
class RootFragment : Fragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_root, container, false)

        //disable default color theme for icon
        view.findViewById<TabLayout>(R.id.tab_layout).tabIconTint = null

        view.findViewById<ViewPager2>(R.id.view_pager).adapter =
            ViewPagerAdapter(requireContext() as FragmentActivity)
        TabLayoutMediator(
            view.findViewById<TabLayout>(R.id.tab_layout),
            view.findViewById<ViewPager2>(R.id.view_pager)
        ) { tab, pos ->
            when (pos) {
                0 -> {
                    tab.setIcon(R.drawable.ic_baseline_person_24)
                }
                1 -> {
                    tab.setIcon(R.drawable.ic_baseline_group_24)
                }
            }
        }.attach()
        return view
    }
}