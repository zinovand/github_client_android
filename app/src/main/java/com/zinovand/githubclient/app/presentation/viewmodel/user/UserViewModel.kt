package com.zinovand.githubclient.app.presentation.viewmodel.user

import androidx.paging.PagingData
import com.zinovand.githubclient.domain.model.user.UsersDto
import kotlinx.coroutines.flow.Flow

/**
 * @author Zinovyev Andrey
 */
interface UserViewModel {

    fun getUsers(): Flow<PagingData<UsersDto>>

    fun getUserDetails(username: String)
}