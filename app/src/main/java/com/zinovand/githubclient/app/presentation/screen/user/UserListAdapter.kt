package com.zinovand.githubclient.app.presentation.screen.user

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zinovand.githubclient.R
import com.zinovand.githubclient.domain.model.user.UsersDto

/**
 * Users recyclerview adapter
 *
 * @author Zinovyev Andrey
 */
class UserListAdapter(private val listener: OnItemClickListener): PagingDataAdapter<UsersDto, UserListAdapter.Holder>(UsersDiffCallback()){

    interface OnItemClickListener{
        fun onItemClick(username: String)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val user = getItem(position) ?: return

        //For local storage only
        //holder.itemView.findViewById<ImageView>(R.id.user_avatar).setImageUri(Uri.parse(user.avatar_url))

        holder.setData(
            user.avatar_url,
            user.id.toString(),
            user.login
        )

        holder.itemView.setOnClickListener{
            listener.onItemClick(getItem(holder.absoluteAdapterPosition)!!.login)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.user_list_rv_item, parent, false)
        return Holder(view)
    }

    class Holder(view: View): RecyclerView.ViewHolder(view){

        fun setData(
            avatar_url: String,
            id: String,
            username: String
        )
        {
            Picasso.get().load(avatar_url).into(itemView.findViewById<ImageView>(R.id.organization_avatar))
            itemView.findViewById<TextView>(R.id.organization_id).text = id
            itemView.findViewById<TextView>(R.id.organization_login).text = username
        }


    }

    class UsersDiffCallback : DiffUtil.ItemCallback<UsersDto>() {
        override fun areItemsTheSame(oldItem: UsersDto, newItem: UsersDto): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: UsersDto, newItem: UsersDto): Boolean {
            return oldItem == newItem
        }
    }

}