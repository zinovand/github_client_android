package com.zinovand.githubclient.app.presentation.viewmodel.organization

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zinovand.githubclient.domain.model.organization.OrganizationsDto
import com.zinovand.githubclient.domain.usecase.organization.OrganizationService
import com.zinovand.githubclient.domain.usecase.organization.OrganizationServiceImpl
import kotlinx.coroutines.launch

/**
 * @author Zinovyev Andrey
 */
class OrganizationViewModelImpl: ViewModel(), OrganizationViewModel {
    private val organizationService: OrganizationService = OrganizationServiceImpl()

    val organizationsList: MutableLiveData<List<OrganizationsDto>> = MutableLiveData()


    override fun getOrganizations(){
        viewModelScope.launch{
            organizationsList.value = organizationService.getOrganizations()
        }
    }
}