package com.zinovand.githubclient.app.presentation.screen.user

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zinovand.githubclient.R
import com.zinovand.githubclient.domain.model.user.UserDto

class UserDetailsAdapter: RecyclerView.Adapter<UserDetailsAdapter.UserDetailsViewHolder>() {
    lateinit var user: UserDto

class UserDetailsViewHolder(view: View): RecyclerView.ViewHolder(view){
    fun setData(
        avatar: String,
        id: String,
        username: String,
        name: String,
        followers: String,
        following: String,
        created: String)
    {
        Picasso.get().load(avatar).into(itemView.findViewById<ImageView>(R.id.user_details_avatar))
        itemView.findViewById<TextView>(R.id.user_details_id).text = id
        itemView.findViewById<TextView>(R.id.user_details_login).text = username
        itemView.findViewById<TextView>(R.id.user_details_name).text = name
        itemView.findViewById<TextView>(R.id.user_details_followers).text = followers
        itemView.findViewById<TextView>(R.id.user_details_following).text = following
        itemView.findViewById<TextView>(R.id.user_details_created).text = created
    }
}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserDetailsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.user_details_rv_item, parent, false)
        return UserDetailsViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserDetailsViewHolder, position: Int) {
        holder.setData(
            user.avatar_url,
            user.id.toString(),
            user.login,
            user.name,
            user.followers.toString(),
            user.following.toString(),
            user.created_at
            )
    }

    override fun getItemCount(): Int {
        return 1
    }


    fun update(userDto: UserDto){
        user = userDto
        notifyDataSetChanged()
    }

}