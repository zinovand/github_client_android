package com.zinovand.githubclient.app.presentation.screen.navigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.zinovand.githubclient.app.presentation.screen.organization.OrganizationFragment
import com.zinovand.githubclient.app.presentation.screen.user.UserFragment

/**
 * Bottom Menu settings
 * 
 * @author Zinovyev Andrey
 */
class ViewPagerAdapter(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> UserFragment()
            else -> OrganizationFragment()
        }
    }
}