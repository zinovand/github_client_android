package com.zinovand.githubclient.app.presentation.viewmodel.organization

interface OrganizationViewModel {

    fun getOrganizations()
}