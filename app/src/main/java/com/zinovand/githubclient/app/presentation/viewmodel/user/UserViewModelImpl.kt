package com.zinovand.githubclient.app.presentation.viewmodel.user

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.zinovand.githubclient.domain.model.user.UserDto
import com.zinovand.githubclient.domain.model.user.UsersDto
import com.zinovand.githubclient.domain.usecase.user.UserService
import com.zinovand.githubclient.domain.usecase.user.UserServiceImpl
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

/**
 * Users list use Flow
 * User details use LiveData
 *
 * @author Zinovyev Andrey
 */
class UserViewModelImpl: ViewModel(), UserViewModel {

    private val userService: UserService = UserServiceImpl()

    private val usersFlow: Flow<PagingData<UsersDto>>

    private val searchBy = MutableLiveData("")

    val userDetails: MutableLiveData<UserDto> = MutableLiveData()

    init {
        usersFlow = searchBy.asFlow()
            .debounce(500)
            .flatMapLatest { userService.getUsers() }
            .cachedIn(viewModelScope)
    }

    override fun getUserDetails(username: String){
        viewModelScope.launch {
            userDetails.value = userService.getUser(username)
            var xx = 33
        }
    }

    /**
     * Use this in fragment for subscribing
     */
    override fun getUsers(): Flow<PagingData<UsersDto>> {
        return usersFlow
    }

}
