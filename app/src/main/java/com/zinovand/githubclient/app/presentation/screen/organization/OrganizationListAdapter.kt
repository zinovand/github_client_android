package com.zinovand.githubclient.app.presentation.screen.organization

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zinovand.githubclient.R
import com.zinovand.githubclient.domain.model.organization.OrganizationsDto

/**
 * Organization recyclerview adapter
 *
 * @author Zinovyev Andrey
 */
class OrganizationListAdapter: RecyclerView.Adapter<OrganizationListAdapter.OrganizationViewHolder>(){
    var organizations = emptyList<OrganizationsDto>()

    class OrganizationViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun setData(
            avatar_url: String,
            id: String,
            login: String,
            description: String
        ){
            Picasso.get().load(avatar_url).into(itemView.findViewById<ImageView>(R.id.organization_avatar))
            itemView.findViewById<TextView>(R.id.organization_id).text = id
            itemView.findViewById<TextView>(R.id.organization_login).text = login
            itemView.findViewById<TextView>(R.id.organization_desc).text = description
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrganizationListAdapter.OrganizationViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.organization_list_rv_item, parent, false)
        return OrganizationListAdapter.OrganizationViewHolder(view)
    }

    override fun onBindViewHolder(holder: OrganizationListAdapter.OrganizationViewHolder, position: Int) {
        val organization = organizations.get(position)

        holder.setData(
            organization.avatar_url,
            organization.id.toString(),
            organization.login,
            organization.description
        )
    }

    override fun getItemCount(): Int {
        return organizations.size
    }

    fun update(list: List<OrganizationsDto>){
        organizations = list.reversed()
        notifyDataSetChanged()
    }

}