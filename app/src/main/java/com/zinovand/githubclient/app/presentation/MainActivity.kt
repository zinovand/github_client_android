package com.zinovand.githubclient.app.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.GestureDetector
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.zinovand.githubclient.R

/**
 * @author Zinovyev Andrey
 */
//class MainActivity : AppCompatActivity(), GestureDetector.OnGestureListener {
//class MainActivity : AppCompatActivity(), View.OnTouchListener {
class MainActivity: AppCompatActivity(){
//    //TODO: REWRITE
//lateinit var swipeListener: OnSwipeTouchListener
//    private var x1: Float = 0.0f
//    private var y1: Float = 0.0f
//    private var x2: Float = 0.0f
//    private var y2: Float = 0.0f
//    private lateinit var gestureDetector: GestureDetector
//
//    companion object {
//        //const val MIN_DISTANCE = 150
//        const val SWIPE_THRESHOLD = 100
//        const val SWIPE_VELOCITY_THRESHOLD = 100
//    }
//    //


    val selectedBack: MutableLiveData<String> = MutableLiveData()

    //lateinit var titleActionBar: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //gestureDetector = GestureDetector(this, this)
        //gestureDetector = GestureDetector(this, GestureListener())
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        selectedBack.value = supportActionBar?.title.toString()
        return item.itemId == android.R.id.home
    }

    fun setActionBarTitle(title: String) {
        supportActionBar?.title = title
    }

    fun actionBarBackEnabled(state: Boolean) {
        supportActionBar?.setDisplayHomeAsUpEnabled(state)
    }

//    private class OnSwipeTouchListener(view: View) : View.OnTouchListener{
//
//
//        private val gestureDetector: GestureDetector
//
//        companion object {
//            private val SWIPE_THRESHOLD = 100
//            private val SWIPE_VELOCITY_THRESHOLD = 100
//        }
//
//        init {
//            gestureDetector = GestureDetector(ctx, GestureListener())
//        }
//
//        override fun onTouch(v: View, event: MotionEvent): Boolean {
//            return gestureDetector.onTouchEvent(event)
//        }
//
//        private inner class GestureListener : GestureDetector.SimpleOnGestureListener() {
//
//
//            override fun onDown(e: MotionEvent): Boolean {
//                return true
//            }
//
//            override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
//                var result = false
//                try {
//                    val diffY = e2.y - e1.y
//                    val diffX = e2.x - e1.x
//                    if (Math.abs(diffX) > Math.abs(diffY)) {
//                        if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
//                            if (diffX > 0) {
//                                onSwipeRight()
//                            } else {
//                                onSwipeLeft()
//                            }
//                            result = true
//                        }
//                    } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
//                        if (diffY > 0) {
//                            onSwipeBottom()
//                        } else {
//                            onSwipeTop()
//                        }
//                        result = true
//                    }
//                } catch (exception: Exception) {
//                    exception.printStackTrace()
//                }
//
//                return result
//            }
//
//
//        }
//
//        open fun onSwipeRight() {}
//
//        open fun onSwipeLeft() {}
//
//        open fun onSwipeTop() {}
//
//        open fun onSwipeBottom() {}
//
//    }







//    ///TODO: REWRITE
//    override fun onTouch(v: View, event: MotionEvent): Boolean{
//        return gestureDetector.onTouchEvent(event)
//    }
//
//    private inner class GestureListener: GestureDetector.SimpleOnGestureListener(){
//        override fun onDown(e: MotionEvent): Boolean {
//            return true
//        }
//
//        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
//            var result = false
//            try {
//                val diffY = e2.y - e1.y
//                val diffX = e2.x - e1.x
//                if (abs(diffX) > abs(diffY)) {
//                    if (abs(diffX) > SWIPE_THRESHOLD && abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
//                        if (diffX > 0) {
//                            onSwipeRight()
//                        } else {
//                            onSwipeLeft()
//                        }
//                        result = true
//                    }
//                } else if (abs(diffY) > SWIPE_THRESHOLD && abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
//                    if (diffY > 0) {
//                        onSwipeBottom()
//                    } else {
//                        onSwipeTop()
//                    }
//                    result = true
//                }
//            } catch (exception: Exception) {
//                exception.printStackTrace()
//            }
//
//            return result
//        }
//
//
//    }
//
//    fun onSwipeRight() {}
//
//    fun onSwipeLeft() {}
//
//    fun onSwipeTop() {setActionBarTitle("TOP")}
//
//    fun onSwipeBottom() {setActionBarTitle("DOWN")}
//    ///


    ///TODO: REWRITE
//
//    override fun onDown(p0: MotionEvent?): Boolean {
//        return false
//    }
//
//    override fun onShowPress(p0: MotionEvent?) {
//        //TODO("Not yet implemented")
//    }
//
//    override fun onSingleTapUp(p0: MotionEvent?): Boolean {
//        return false
//    }
//
//    override fun onScroll(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float): Boolean {
//        return false
//    }
//
//    override fun onLongPress(p0: MotionEvent?) {
//        TODO("Not yet implemented")
//    }
//
//    override fun onFling(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float): Boolean {
//        return false
//    }
//
//    override fun onTouchEvent(event: MotionEvent?): Boolean {
//        gestureDetector.onTouchEvent(event)
//
//        when (event?.action) {
//
//            //When start swipe
//            0 -> {
//                x1 = event.x
//                y1 = event.y
//            }
//
//            //When end swipe
//            1 -> {
//                x2 = event.x
//                y2 = event.y
//
//                val valueY: Float = y2 - y1
//
//                if (abs(valueY) > MIN_DISTANCE) {
//                    if (y2 > y1) {
//                        Toast.makeText(this, "Bottom swipe", Toast.LENGTH_SHORT).show()
//                    }
//
//                    else{
//                        Toast.makeText(this, "Top swipe", Toast.LENGTH_SHORT).show()
//                    }
//
//                }
//            }
//        }
//
//        return super.onTouchEvent(event)
//    }
    ///
}