package com.zinovand.githubclient.app.presentation.screen.user

//import OnSwipeTouchListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.zinovand.githubclient.R
import com.zinovand.githubclient.app.presentation.MainActivity
import com.zinovand.githubclient.app.presentation.viewmodel.user.UserViewModelImpl
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


/**
 * @author Zinovyev Andrey
 */
class UserFragment() : Fragment(), UserListAdapter.OnItemClickListener {
    lateinit var recyclerView: RecyclerView
    lateinit var listAdapter: UserListAdapter

    lateinit var userDetailsAdapter: UserDetailsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewModel = ViewModelProvider(this).get(UserViewModelImpl::class.java)
        val view = inflater.inflate(R.layout.fragment_user, container, false)
        recyclerView = view.findViewById(R.id.rv_user)
        listAdapter = UserListAdapter(this)
        userDetailsAdapter = UserDetailsAdapter()
        recyclerView.adapter = listAdapter

        (activity as MainActivity).actionBarBackEnabled(false)
        (activity as MainActivity).setActionBarTitle("Users")

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.getUsers().collectLatest {
                listAdapter.submitData(it)
            }
        }

            viewModel.userDetails.observe(
            viewLifecycleOwner
            ) {
                (activity as MainActivity).setActionBarTitle("Profile " + it.login)
                (activity as MainActivity).actionBarBackEnabled(true)
                recyclerView.adapter = userDetailsAdapter
                userDetailsAdapter.update(it)
            }

        (activity as MainActivity).selectedBack.observe(
            viewLifecycleOwner
        ) {
            recyclerView.adapter = listAdapter
            (activity as MainActivity).actionBarBackEnabled(false)
            (activity as MainActivity).setActionBarTitle("Users")
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).setActionBarTitle("Users")
    }

    override fun onItemClick(username: String) {
        val viewModel = ViewModelProvider(this).get(UserViewModelImpl::class.java)
        viewModel.getUserDetails(username)
    }
}
