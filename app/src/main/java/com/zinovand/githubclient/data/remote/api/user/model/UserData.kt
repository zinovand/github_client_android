package com.zinovand.githubclient.data.remote.api.user.model

/**
 * Selected user details
 *
 * @author Zinovyev Andrey
 */
data class UserData(
    val avatar_url: String, //need
    val bio: Any,
    val blog: String,
    val company: String,
    val created_at: String, //need
    val email: String?, //need
    val events_url: String,
    val followers: Int, //need
    val followers_url: String,
    val following: Int, //need
    val following_url: String,
    val gists_url: String,
    val gravatar_id: String,
    val hireable: Any,
    val html_url: String,
    val id: Int, //need
    val location: String,
    val login: String, //need
    val name: String?, //need
    val node_id: String,
    val organizations_url: String,
    val public_gists: Int,
    val public_repos: Int,
    val received_events_url: String,
    val repos_url: String,
    val site_admin: Boolean,
    val starred_url: String,
    val subscriptions_url: String,
    val twitter_username: Any,
    val type: String,
    val updated_at: String,
    val url: String
)