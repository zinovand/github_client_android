package com.zinovand.githubclient.data.remote.api

import com.zinovand.githubclient.data.remote.api.organization.OrganizationApi
import com.zinovand.githubclient.data.remote.api.user.UserApi

/**
 * @author Zinovyev Andrey
 */
interface ApiService: UserApi, OrganizationApi