package com.zinovand.githubclient.data.remote.api.organization

import com.zinovand.githubclient.data.remote.api.organization.model.OrganizationsData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Zinovyev Andrey
 */
interface OrganizationApi {

    /**
     *Lists all organizations, in the order that they were created on GitHub.
     * Note: Pagination is powered exclusively by the since parameter.
     *
     * @param since An organization ID. Only return organizations with an ID greater than this ID.
     * @param per_page The number of results per page (max 100). Default: 30
     */
    @GET("./organizations")
    suspend fun getOrganizations(
        @Query("since") sinceOrganizationId: Int,
        @Query("per_page") organizationsPerPage: Int
    ): Response<OrganizationsData>
}