package com.zinovand.githubclient.data.remote.api.user.model

/**
 * All users list
 *
 * @author Zinovyev Andrey
 */
class UsersData: ArrayList<UsersItemData>()