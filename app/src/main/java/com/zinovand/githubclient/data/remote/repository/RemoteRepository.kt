package com.zinovand.githubclient.data.remote.repository

import androidx.paging.PagingData
import com.zinovand.githubclient.data.remote.api.organization.model.OrganizationsData
import com.zinovand.githubclient.data.remote.api.user.model.UserData
import com.zinovand.githubclient.data.remote.api.user.model.UsersData
import com.zinovand.githubclient.data.remote.api.user.model.UsersItemData
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

/**
 * Interface for usage in domain module
 *
 * @author Zinovyev Andrey
 */
interface RemoteRepository {
    suspend fun getUsers(sinceUserId: Int = 0,
                         usersPerPage: Int = 50): Response<UsersData>

    suspend fun getUser(username: String): Response<UserData>

    suspend fun getOrganizations(sinceOrganizationId: Int = 0,
                                 organizationsPerPage: Int = 30): Response<OrganizationsData>
}