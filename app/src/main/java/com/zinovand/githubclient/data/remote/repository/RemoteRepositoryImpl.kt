package com.zinovand.githubclient.data.remote.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.zinovand.githubclient.data.remote.RetrofitInstance
import com.zinovand.githubclient.data.remote.api.organization.model.OrganizationsData
import com.zinovand.githubclient.data.remote.api.user.model.UserData
import com.zinovand.githubclient.data.remote.api.user.model.UsersData
import com.zinovand.githubclient.domain.usecase.user.UsersPagingSource
import retrofit2.Response

/**
 * Interface for usage in domain module
 */
class RemoteRepositoryImpl: RemoteRepository{

    override suspend fun getUsers(sinceUserId: Int, usersPerPage: Int): Response<UsersData>
    {
        return RetrofitInstance.githubApi.getUsers(sinceUserId, usersPerPage)
    }

    override suspend fun getUser(username: String): Response<UserData> {
        return RetrofitInstance.githubApi.getUser(username = username)
    }

    override suspend fun getOrganizations(sinceOrganizationId: Int, organizationsPerPage: Int): Response<OrganizationsData> {
        return RetrofitInstance.githubApi.getOrganizations(sinceOrganizationId, organizationsPerPage)
    }
}