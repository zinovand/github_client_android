package com.zinovand.githubclient.data.remote.api.user

import com.zinovand.githubclient.data.remote.api.user.model.UserData
import com.zinovand.githubclient.data.remote.api.user.model.UsersData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author Zinovyev Andrey
 */
interface UserApi {

    /**
     * Lists all users, in the order that they signed up on GitHub. This list includes personal user accounts and organization accounts.
     * Note: Pagination is powered exclusively by the since parameter.
     *
     * @param since A user ID. Only return users with an ID greater than this ID.
     * @param per_page The number of results per page (max 100). Default: 30
     */
    @GET("./users")
    suspend fun getUsers(
        @Query("since") sinceUserId: Int,
        @Query("per_page") usersPerPage: Int
    ): Response<UsersData>


    @GET("users/{username}")
    suspend fun getUser(
        @Path("username") username: String
    ): Response<UserData>
}