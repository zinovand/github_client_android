package com.zinovand.githubclient.data.remote

import com.zinovand.githubclient.data.remote.api.ApiService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {
    private val GITHUB_URL = "https://api.github.com"

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(GITHUB_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val githubApi: ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }
}