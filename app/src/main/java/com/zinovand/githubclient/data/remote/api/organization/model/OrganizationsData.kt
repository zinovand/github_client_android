package com.zinovand.githubclient.data.remote.api.organization.model

/**
 * @author Zinovyev Andrey
 */
class OrganizationsData : ArrayList<OrganizationsItemData>()