package com.zinovand.githubclient.domain.model.user

/**
 * Model for UserDetails
 *
 * @author Zinovyev Andrey
 */
data class UserDto(
    val id: Int,
    val avatar_url: String,
    val login: String,
    val name: String,
    val email: String,
    val followers: Int,
    val following: Int,
    val created_at: String,
)