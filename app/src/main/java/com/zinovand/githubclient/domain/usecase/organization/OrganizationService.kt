package com.zinovand.githubclient.domain.usecase.organization

import com.zinovand.githubclient.domain.model.organization.OrganizationsDto

/**
 * @author Zinovyev Andrey
 */
interface OrganizationService {
    suspend fun getOrganizations(): List<OrganizationsDto>
}