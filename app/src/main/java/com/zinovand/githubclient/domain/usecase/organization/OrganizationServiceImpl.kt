package com.zinovand.githubclient.domain.usecase.organization

import com.zinovand.githubclient.data.remote.repository.RemoteRepositoryImpl
import com.zinovand.githubclient.domain.model.convertor.ConvertorServiceImpl
import com.zinovand.githubclient.domain.model.organization.OrganizationsDto

/**
 * @author Zinovyev Andrey
 */
class OrganizationServiceImpl : OrganizationService {

    private val repository = RemoteRepositoryImpl()

    private var lastId: Int = 0

    override suspend fun getOrganizations(): List<OrganizationsDto> {
        val response = repository.getOrganizations(lastId)
        lastId = response.body()!!.last().id

        return ConvertorServiceImpl.convertDataToDto(response.body()!!)
    }
}