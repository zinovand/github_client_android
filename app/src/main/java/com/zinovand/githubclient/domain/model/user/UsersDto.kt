package com.zinovand.githubclient.domain.model.user

/**
 * Single user line for recycler view
 *
 * @author Zinovyev Andrey
 */
data class UsersDto(
    val avatar_url: String,
    val id: Int,
    val login: String
)