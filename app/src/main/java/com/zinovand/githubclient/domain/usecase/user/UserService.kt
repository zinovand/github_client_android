package com.zinovand.githubclient.domain.usecase.user

import androidx.paging.PagingData
import com.zinovand.githubclient.domain.model.user.UserDto
import com.zinovand.githubclient.domain.model.user.UsersDto
import kotlinx.coroutines.flow.Flow

/**
 * @author Zinovyev Andrey
 */
interface UserService {
    suspend fun getUsers(): Flow<PagingData<UsersDto>>

    suspend fun getUser(username: String): UserDto
}