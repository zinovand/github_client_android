package com.zinovand.githubclient.domain.model.convertor

import com.zinovand.githubclient.data.remote.api.organization.model.OrganizationsData
import com.zinovand.githubclient.domain.model.organization.OrganizationsDto

/**
 * @author Zinovyev Andrey
 */
interface OrganizationConvertor {
    fun convertDataToDto(organization: OrganizationsData) : ArrayList<OrganizationsDto>
}