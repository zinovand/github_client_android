package com.zinovand.githubclient.domain.model.organization

/**
 * Single organization line for recycler view
 *
 * @author Zinovyev Andrey
 */
data class OrganizationsDto(
    val avatar_url: String,
    val id: Int,
    val login: String,
    val description: String
)