package com.zinovand.githubclient.domain.model.convertor

import com.zinovand.githubclient.data.remote.api.organization.model.OrganizationsData
import com.zinovand.githubclient.data.remote.api.user.model.UserData
import com.zinovand.githubclient.data.remote.api.user.model.UsersData
import com.zinovand.githubclient.domain.model.organization.OrganizationsDto
import com.zinovand.githubclient.domain.model.user.UserDto
import com.zinovand.githubclient.domain.model.user.UsersDto

/**
 * @author Zinovyev Andrey
 */
object ConvertorServiceImpl : ConvertorService {
    override fun convertDataToDto(user: UserData): UserDto {
        return UserDto(
            id = user.id,
            avatar_url = user.avatar_url,
            login = user.login,
            name = getNotNullAttribute(user.name),
            email = getNotNullAttribute(user.email),
            followers = user.followers,
            following = user.following,
            created_at = user.created_at
        )
    }

    override fun convertDataToDto(user: UsersData): ArrayList<UsersDto> {
        var resList: ArrayList<UsersDto> = ArrayList()

        //TODO rewrite use stream
        for (u in user) {
            resList.add(
                UsersDto(
                    avatar_url = u.avatar_url,
                    id = u.id,
                    login = u.login
                )
            )
        }
        return resList
    }

    override fun convertDataToDto(organization: OrganizationsData): ArrayList<OrganizationsDto> {
        var resList: ArrayList<OrganizationsDto> = ArrayList()

        for (o in organization){
            resList.add(
                OrganizationsDto(
                    avatar_url = o.avatar_url,
                    id = o.id,
                    login = o.login,
                    description = getNotNullAttribute(o.description)
                )
            )
        }

        return resList
    }

    /**
     * In Dto model shouldn't be null attributes
     */
    private fun getNotNullAttribute(attr: String?): String{
        return attr ?: " "
    }
}