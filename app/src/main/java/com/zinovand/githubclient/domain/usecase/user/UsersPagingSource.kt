package com.zinovand.githubclient.domain.usecase.user

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.zinovand.githubclient.data.remote.repository.RemoteRepository
import com.zinovand.githubclient.domain.model.convertor.ConvertorServiceImpl
import com.zinovand.githubclient.domain.model.user.UsersDto

/**
 * @author Zinovyev Andrey
 */
class UsersPagingSource(private val repository: RemoteRepository, ) : PagingSource<Int, UsersDto>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, UsersDto> {
        val pageId: Int = params.key ?: 0
        val pageSize: Int = params.loadSize

        return try {
            val users = ConvertorServiceImpl.convertDataToDto(repository.getUsers(pageId, params.loadSize).body()!!)
            return LoadResult.Page(
                data = users,
                nextKey  = if (users.size < pageSize) null else users.last().id + 1,
                prevKey = if (pageId == 0) null else users.first().id - 1
            )
        } catch (e: Exception) {
            LoadResult.Error(throwable = e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, UsersDto>): Int? {
        val anchorPosition = state.anchorPosition ?: return null
        val page = state.closestPageToPosition(anchorPosition) ?: return null
        return page.prevKey?.plus(1) ?: page.nextKey?.minus(1)
    }

}