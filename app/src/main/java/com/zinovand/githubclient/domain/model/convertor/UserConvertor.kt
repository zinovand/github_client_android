package com.zinovand.githubclient.domain.model.convertor

import com.zinovand.githubclient.data.remote.api.user.model.UserData
import com.zinovand.githubclient.data.remote.api.user.model.UsersData
import com.zinovand.githubclient.domain.model.user.UserDto
import com.zinovand.githubclient.domain.model.user.UsersDto

/**
 * @author Zinovyev Andrey
 */
interface UserConvertor {
    fun convertDataToDto(user: UserData) : UserDto

    fun convertDataToDto(user: UsersData) : ArrayList<UsersDto>
}