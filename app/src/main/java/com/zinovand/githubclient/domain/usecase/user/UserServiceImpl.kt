package com.zinovand.githubclient.domain.usecase.user

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.zinovand.githubclient.data.remote.repository.RemoteRepositoryImpl
import com.zinovand.githubclient.domain.model.convertor.ConvertorServiceImpl
import com.zinovand.githubclient.domain.model.user.UserDto
import com.zinovand.githubclient.domain.model.user.UsersDto
import kotlinx.coroutines.flow.Flow

/**
 * @author Zinovyev Andrey
 */
class UserServiceImpl : UserService {

    private val repository = RemoteRepositoryImpl()

    override suspend fun getUsers(): Flow<PagingData<UsersDto>> =
        Pager(
            config = PagingConfig(pageSize = 30, enablePlaceholders = false),
            pagingSourceFactory = { UsersPagingSource(repository) }
        ).flow

    override suspend fun getUser(username: String): UserDto =
        ConvertorServiceImpl.convertDataToDto(
            repository.getUser(username).body()!!
        )

}