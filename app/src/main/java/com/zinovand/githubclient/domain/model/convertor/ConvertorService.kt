package com.zinovand.githubclient.domain.model.convertor

/**
 * @author Zinovyev Andrey
 */
interface ConvertorService : UserConvertor, OrganizationConvertor{

}